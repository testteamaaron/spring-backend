package swiss.models;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

public class PlayerTest {


    @Test
    public void testPMW() {
        Player playerA = new Player("a");
        playerA.score(2, 0, 0);
        playerA.score(2, 0, 0);
        playerA.score(2, 0, 0);
        playerA.score(2, 0, 0);
        playerA.score(2, 0, 0);
        playerA.score(0, 2, 0);
        playerA.score(0, 2, 0);
        playerA.score(0, 0, 1);
        assertThat(playerA.playerMatchesWon(), is(0.667));

        Player playerB = new Player("b");
        playerB.score(1, 0, 1);
        playerB.score(0, 1, 1);
        playerB.score(0, 1, 1);
        playerB.score(0, 1, 1);
        assertThat(playerB.playerMatchesWon(), is (0.33));


        Player playerC = new Player("c");
        playerC.score(1,0, 1);
        playerC.score(1,0, 1);
        playerC.score(1,0, 1);
        playerC.score(0,1, 1);
        playerC.score(0,1, 1);
        assertThat(playerC.playerMatchesWon(), is (0.60));
    }

    @Test
    public void testPGW() {
        Player playerA = new Player("a");
        playerA.score(2, 0, 0);
        playerA.score(2, 1, 0);
        playerA.score(1, 2, 0);
        playerA.score(2, 0, 0);
        assertThat(playerA.getPlayerGamesWon(), is(0.70));

        Player playerB = new Player("b");
        playerB.score(1, 2, 0);
        playerB.score(1, 2, 0);
        playerB.score(0, 2, 0);
        playerB.score(1, 2, 0);

        assertThat(playerB.getPlayerGamesWon(), is(0.33));

        Player playerC = new Player("c");
        playerC.score(2,0, 1);
        playerC.score(1,0, 0);
        assertThat(playerC.getPlayerGamesWon(), is (0.833));
    }

    @Test
    public void testOMW() {
        Player playerA = new Player("a");
        Player playerB = new Player("a");
        playerA.played.add(playerB);
        playerA.score(2, 0, 0);
        playerB.score(0, 2, 0);
        assertThat(playerA.getOpponentMatchesWon(), is (0.33));


    }


}
