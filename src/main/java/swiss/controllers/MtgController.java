package swiss.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.*;
import swiss.models.GameResult;
import swiss.models.Pairing;
import swiss.models.Player;
import swiss.models.Tournament;
import swiss.services.TournamentResolver;

import javax.websocket.server.PathParam;
import java.util.List;
import java.util.UUID;

@RestController
@EnableAutoConfiguration
@ComponentScan
public class MtgController {

    @Autowired
    private TournamentResolver tournamentResolver;

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public Tournament createTournament() {
        return tournamentResolver.addTournament();
    }

    @RequestMapping(value = "/{uuid}", method = RequestMethod.GET)
    public Tournament getTournament(@ModelAttribute("uuid") UUID uuid) {
        return tournamentResolver.getTournament(uuid);
    }

    @RequestMapping(value = "/{uuid}/player", method = RequestMethod.POST)
    public Tournament addPlayer(@ModelAttribute("uuid") UUID uuid, @RequestParam("person") String player) {
        Tournament tournament = tournamentResolver.getTournament(uuid);
        if (player != null && !player.isEmpty() && !tournament.getPlayers().contains(player))
        {
            tournament.addPlayer(player);
        }
        return tournament;
    }

    @RequestMapping(value = "/{uuid}", method = RequestMethod.POST)
    public Tournament finalisePlayers(@ModelAttribute("uuid") UUID uuid) {
        Tournament tournament = tournamentResolver.getTournament(uuid);
        tournament.start();
        return tournament;
    }

    @RequestMapping("/{uuid}/pairings")
    public List<Pairing> getPairingsForRound(@ModelAttribute("uuid") UUID uuid) {
        Tournament tournament = tournamentResolver.getTournament(uuid);
        return tournament.getCurrRound().getPairings();
    }

    @RequestMapping(value = "/{uuid}/pairings/{pairId}", method = RequestMethod.POST)
    public Tournament reportResult(@PathVariable UUID uuid, @PathVariable String pairId,
                             @RequestBody GameResult result) {
        Tournament tournament = tournamentResolver.getTournament(uuid);
        tournament.getCurrRound().reportResult(pairId, result);
        return tournament;
    }

    @RequestMapping(value = "/{uuid}/pairings", method = RequestMethod.POST)
    public Tournament completeRound(@ModelAttribute("uuid") UUID uuid) {
        Tournament tournament = tournamentResolver.getTournament(uuid);
        tournament.completeRound();
        return tournament;
    }

    @RequestMapping("/{uuid}")
    public List<Player> getResult(@ModelAttribute("uuid") UUID uuid) {
        Tournament tournament = tournamentResolver.getTournament(uuid);
        return tournament.calcRankings();
    }
}
