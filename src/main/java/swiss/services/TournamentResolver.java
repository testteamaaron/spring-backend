package swiss.services;

import com.google.common.collect.Maps;
import swiss.models.Tournament;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.UUID;

@Service
public class TournamentResolver {
    // TODO guava cache.
    private HashMap<UUID, Tournament> tournamentMap;


    public TournamentResolver() {
        tournamentMap = Maps.newHashMap();
    }

    public Tournament getTournament(UUID uuid) {
        Tournament tournament = tournamentMap.get(uuid);
        if (tournament == null) {
            throw new IllegalArgumentException("No such tournament.");
        }
        return tournament;
    }

    public Tournament addTournament() {
        UUID uuid = UUID.randomUUID();
        Tournament tournament = new Tournament(uuid);
        tournamentMap.put(uuid, tournament);
        return tournament;
    }

    public void addTournament(UUID uuid) {
        if (tournamentMap.containsKey(uuid)) {
            throw new IllegalArgumentException("Cannot overwrite existing tourns");
        }
        tournamentMap.put(uuid, new Tournament());
    }
}
