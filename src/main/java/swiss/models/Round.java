package swiss.models;

import com.google.common.collect.Lists;

import java.util.*;

public class Round {
    private Map<String, Pairing> pairingsMap = new LinkedHashMap<>();
    private static Random rand = new Random();

    public Round(List<Pairing> pairings) {
        pairings.forEach(pairing -> pairingsMap.put(pairing.getPairId(), pairing));
    }

    public List<Pairing> getPairings() {
        return Lists.newArrayList(pairingsMap.values());
    }

    public static Round createFirstRound(List<Player> players) {
        List<Player> playersLeft = Lists.newArrayList(players);
        List<Pairing> pairs = new ArrayList<>();

        while (playersLeft.size() > 1) {
            Player player1 = playersLeft.remove(rand.nextInt(playersLeft.size()));
            Player player2 = playersLeft.remove(rand.nextInt(playersLeft.size()));
            pairs.add(new Pairing(player1, player2));
        }
        return new Round(pairs);
    }

    public static Round createRound(List<Player> players) {

        // We do the pairings here. Pair by number of points.
        List<Player> playersLeft = Lists.newArrayList(players);
        List<Pairing> pairs = new ArrayList<>();
        playersLeft.sort((Player p1, Player p2) -> Integer.compare(p2.getPoints(), p1.getPoints()));
        while (playersLeft.size() > 1) {
            Player p1 = playersLeft.remove(0);
            for (Player p : playersLeft) {
                if (!p1.played.contains(p)) {
                    Player p2 = p;
                    playersLeft.remove(p);
                    pairs.add(new Pairing(p1, p2));
                    break;
                }
            }
        }
        return new Round(pairs);
    }

    public void reportResult(String id, GameResult result) {

        // More validation required.
        Pairing pairing = pairingsMap.get(id);
        if (pairing != null) {
            pairing.report(result);
        }

    }

    public void finishRound() {
        for (Pairing p : pairingsMap.values()) {
            if (!p.reported) {
                throw new RuntimeException("Can't finish round yet");
            }
        }

        pairingsMap.values().forEach(pairing -> pairing.getP1().played.add(pairing.getP2()));
        pairingsMap.values().forEach(pairing -> pairing.getP2().played.add(pairing.getP1()));

        for (Pairing p : pairingsMap.values()) {
            p.finalizeReport();
        }
    }
}
