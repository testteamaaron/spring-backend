package swiss.models;

public class Pairing {
    private Player p1;
    private Player p2;
    private String pairId;

   GameResult result = new GameResult();
    boolean reported = false;

    public Pairing(Player p1, Player p2) {
        this.p1 = p1;
        this.p2 = p2;
        pairId = p1.getName() + p2.getName();
    }

    public Player getP1()
    {
        return p1;
    }

    public Player getP2()
    {
        return p2;
    }

    public GameResult getResult() {
        return result;
    }

    public String getPairId() {
        return pairId;
    }

    public Player getWinner() {
        if (result.getP1Win() == 2) {
            return p1;
        }
        else if (result.getP2Win() == 2) {
            return p2;
        }
        else {
            return null;
        }
    }

    public void report(GameResult result) {
        this.result = result;
        reported = true;
    }

    public void finalizeReport() {
        p1.score(result.getP1Win(), result.getP2Win(), result.getDraw());
        p2.score(result.getP2Win(), result.getP1Win(), result.getDraw());
    }
}
