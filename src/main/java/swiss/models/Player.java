package swiss.models;

import org.apache.commons.math3.util.Precision;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class Player {

    public Player(String name) {
        this.name = name;
    }

    private String name;
    List<Player> played = new ArrayList<>();
    private int mWon = 0;
    private int mLost = 0;
    private int mDraw = 0;

    private int gWon = 0;
    private int gLost = 0;
    private int gDraw = 0;

    void score(int won, int lost, int draw) {
        if (won > lost ) {
            mWon++;
        }
        else if (lost > won) {
            mLost ++;
        }
        else {
            mDraw++;
        }

        this.gWon += won;
        this.gDraw += draw;
        this.gLost += lost;
    }

    public String getName()
    {
        return name;
    }

    // points and player matches are basically the same thing.
    public int getPoints() {
        return mWon*3 + mDraw;
    }

    // The math for how to compute PMW is done here.
    // http://www.wizards.com/dci/downloads/MTG_MTR_1Jan10_EN.pdf Appendix D
    public double playerMatchesWon() {
        if (mWon + mLost + mDraw == 0) {
            return 0;
        }

        double pmw = getPoints() / (double)((mWon + mLost + mDraw) * 3);
        return Precision.round(Math.max(0.33, pmw), 3);
    }

    public double getPlayerGamesWon() {
        if (mWon + mLost + mDraw == 0) {
            return 0;
        }

        double pgw = (gWon*3+gDraw) / (double)((gWon + gDraw + gLost)*3);
        return Precision.round(Math.max(0.33, pgw), 3);
    }

    // Note that pgw and pmw uses gwon+gdraw+glost rather than players due to byes. OGW and OMW uses players because a bye
    // aint an opponent.

    public double getOpponentGamesWon() {
        return played.stream().mapToDouble(opponent -> opponent.getPlayerGamesWon()).average().orElse(0);
    }

    public double getOpponentMatchesWon() {
        return played.stream().mapToDouble(opponent -> opponent.playerMatchesWon()).average().orElse(0);
    }

    @Override
    public String toString() {
        return name + " (" + mWon + "-" + mLost + "-" + mDraw + ")";
    }
}
