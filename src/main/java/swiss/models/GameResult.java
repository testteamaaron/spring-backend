package swiss.models;

public class GameResult {
    private int p1Win;
    private int p2Win;
    private int draw;

    public GameResult(int p1Win, int p2Win, int draw) {
        this.p1Win = p1Win;
        this.p2Win = p2Win;
        this.draw = draw;
    }

    public GameResult() {
        p1Win = 0;
        p2Win = 0;
        draw = 0;
    }

    public int getP1Win() {
        return p1Win;
    }

    public int getP2Win() {
        return p2Win;
    }

    public int getDraw() {
        return draw;
    }
}
