package swiss.models;

import java.util.Comparator;

public class PlayerComparator implements Comparator<Player> {

    @Override
    public int compare(Player o1, Player o2) {
        int points = Integer.compare(o2.getPoints(), o1.getPoints());
        if (points != 0) {
            return points;
        }

        int omw = Double.compare(o2.getOpponentMatchesWon(), o1.getOpponentMatchesWon());
        if (omw != 0) {
            return omw;
        }

        int pgw = Double.compare(o2.getPlayerGamesWon(), o1.getPlayerGamesWon());
        if (pgw != 0) {
            return pgw;
        }
        return Double.compare(o2.getOpponentGamesWon(), o1.getOpponentGamesWon());
    }
}
