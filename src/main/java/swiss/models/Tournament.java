package swiss.models;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

public class Tournament {

    private List<Player> players = new ArrayList<>();

    private int currRoundNum;

    private int roundNum;

    private Round currRound;

    private UUID uuid;

    private List<Player> currentStanding;

    private boolean tournamentComplete = false;

    public Tournament()
    {

    }

    public Tournament(UUID uuid)
    {
        this.uuid = uuid;
    }

    public List<Player> getCurrentStanding()
    {
        return currentStanding;
    }

    public UUID getUuid()
    {
        return uuid;
    }

    public void setUuid(UUID uuid)
    {
        this.uuid = uuid;
    }

    public void addPlayer(String player) {
        players.add(new Player(player));
    }

    public List<Player> getPlayers()
    {
        return players;
    }

    public void start() {
        currentStanding = calcRankings();
        currRound = Round.createFirstRound(players);
        currRoundNum = 1;
        roundNum = getRoundsCount();
    }


    public boolean completeRound() {
        if (tournamentComplete) {
            return false;
        }

        currRound.finishRound();
        currentStanding = calcRankings();

        if (roundNum == currRoundNum) {
            tournamentComplete = true;
            return false;
        }
        else {
            Round nextRound = Round.createRound(players);
            currRound = nextRound;
            currRoundNum++;
            return true;
        }
    }

    private int getRoundsCount() {
        if (players.size() <= 8) {
            return 3;
        }
        else if (players.size() <= 16) {
            return 4;
        }
        else if (players.size() <= 32) {
            return 5;
        }
        else if (players.size() <= 64) {
            return 6;
        }
        throw new NotImplementedException();
        // http://www.wizards.com/dci/downloads/MTG_MTR_1Jan10_EN.pdf section F
    }

    public List<Player> calcRankings() {
        return players.stream().sorted(new PlayerComparator()).collect(Collectors.toList());
    }

    public int getCurrRoundNum() {
        return currRoundNum;
    }

    public int getRoundNum() {
        return roundNum;
    }

    public Round getCurrRound() {
        return currRound;
    }

    public boolean isTournamentComplete() {
        return tournamentComplete;
    }

    public void setTournamentComplete(boolean tournamentComplete) {
        this.tournamentComplete = tournamentComplete;
    }
}
